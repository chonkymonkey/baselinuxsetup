set number
set nocompatible
set encoding=utf-8
set noerrorbells
set tabstop=4 
set sts=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set nowrap
set textwidth=0
set wrapmargin=0
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set hls incsearch
filetype plugin on

set colorcolumn=100
highlight ColorColumn ctermbg=0 guibg=lightgrey

filetype plugin indent on
call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree'
  let NERDTreeIgnore = [ '__pycache__', '\.pyc$', '\.o$', '\.swp',  '*\.swp',  'node_modules/' ]
  let NERDTreeShowHidden=1

  autocmd vimenter * NERDTree
  autocmd StdinReadPre * let s:std_in=1
  autocmd VimEnter * if argc() == 0 && !exists("s:stdn_in") | NERDTree | endif
  map <C-n> :NERDTreeToggle<CR>

Plug 'valloric/youcompleteme', { 'do': './install.py' }
  
  " ---> youcompleteme configuration <---
  let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
  
  set completeopt-=preview
" ---> navigating to the definition of a a symbol <---
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

Plug 'ctrlpvim/ctrlp.vim'
" ---> git commands within vim <---
Plug 'tpope/vim-fugitive'
" ---> git changes on the gutter <---
Plug 'airblade/vim-gitgutter'
" ---> nerdtree git changes <---
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'morhetz/gruvbox'
Plug 'alvan/vim-closetag'
" ---> files on which to activate tags auto-closing <---
  let g:closetag_filenames = '*.html,*.xhtml,*.xml,*.vue,*.phtml,*.js,*.jsx,*.coffee,*.erb'
" ---> closing braces and brackets <---
Plug 'jiangmiao/auto-pairs'
Plug 'christoomey/vim-tmux-navigator'
Plug 'lyuts/vim-rtags'
Plug 'mbbill/undotree'
Plug 'jremmen/vim-ripgrep'
Plug 'fatih/vim-go'
Plug 'vim-airline/vim-airline'
Plug 'mileszs/ack.vim'
Plug 'suan/vim-instant-markdown'
Plug 'yuttie/comfortable-motion.vim' 
Plug '907th/vim-auto-save'
Plug 'preservim/nerdcommenter'
call plug#end()

"gruvbox
set bg=dark
colorscheme gruvbox

"git searching
if executable('rg')
    let g:rg_derive_root='true'
endif

"tell fuzzy search to ignore certain files
"let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
"file tree stuff
let g:netrw_browse_split=2
let g:netrw_banner=0
let g:netrw_winsize=25

"let g:ctrlp_use_caching=0
let g:go_fmt_autosave=0
let g:go_imports_autosave=0
let mapleader = " "
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
noremap <leader>u :UndotreeShow<CR>
nnoremap <leader>pv :wincmd v <bar> :Ex <bar> :vertical resize 30<CR>
nnoremap <leader>ps :Rg<SPACE>

nnoremap <silent><leader>gd :YcmCompleter GoTo<CR>
nnoremap <silent><leader>gf :YcmCompleter FixIt<CR>
" ack.vim --- {{{
"
" " Use ripgrep for searching ⚡️
" " Options include:
" " --vimgrep -> Needed to parse the rg response properly for ack.vim
" " --type-not sql -> Avoid huge sql file dumps as it slows down the search
" " --smart-case -> Search case insensitive if all lowercase pattern, Search
" case sensitively otherwise
let g:ackprg = 'rg --vimgrep --type-not sql --smart-case'
"
"" Auto close the Quickfix list after pressing '<enter>' on a list item
let g:ack_autoclose = 1

" Any empty ack search will search for the work the cursor is on
let g:ack_use_cword_for_empty_search = 1
"
"" Don't jump to first match
cnoreabbrev Ack Ack!

" Maps <leader>/ so we're ready to type the search keyword
nnoremap <Leader>/ :Ack!<Space>
" " }}}
"
" " Navigate quickfix list with ease
nnoremap <silent> [q :cprevious<CR>
nnoremap <silent> ]q :cnext<CR>"
" "
" "
" }}}
if executable('rg')
      let g:ctrlp_user_command = 'rg --files %s'
      let g:ctrlp_use_caching = 0
      let g:ctrlp_working_path_mode = 'ra'
      let g:ctrlp_switch_buffer = 'et'
endif
"smooth scroll stuff
let g:comfortable_motion_scroll_down_key = "j"
let g:comfortable_motion_scroll_up_key = "k"
set mouse=a
noremap <silent> <ScrollWheelDown> :call comfortable_motion#flick(20)<CR>
noremap <silent> <ScrollWheelUp>   :call comfortable_motion#flick(-20)<CR>

let g:auto_save = 1  " enable AutoSave on Vim startup"
let g:auto_save_silent = 1  " do not display the auto-save notification"
